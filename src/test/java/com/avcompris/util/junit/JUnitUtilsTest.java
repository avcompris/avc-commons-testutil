package com.avcompris.util.junit;

import static com.avcompris.util.junit.JUnitUtils.createTmpFileFromCommentsAroundThisMethod;
import static com.avcompris.util.junit.JUnitUtils.getCurrentTestMethodAndClassNames;
import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.apache.commons.lang3.StringUtils.normalizeSpace;
import static org.junit.Assert.assertEquals;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import com.avcompris.util.junit.JUnitUtils.TestMethodAndClassNames;

/**
 * test the {@link JUnitUtils} utility class.
 */
public class JUnitUtilsTest {

	@Test
	public void testSimpleCreateXmlFile() throws Exception {

		// <helloWorld/>

		final File file = createTmpFileFromCommentsAroundThisMethod();

		final String content = FileUtils.readFileToString(file, UTF_8);

		assertEquals("<helloWorld/>", content.trim());
	}

	@Test
	public void testGetCurrentTestMethodAndClassNames() throws Exception {

		final TestMethodAndClassNames t = getCurrentTestMethodAndClassNames();

		assertEquals(JUnitUtilsTest.class.getName(), t.className);

		assertEquals("testGetCurrentTestMethodAndClassNames", t.methodName);
	}

	@Test
	public void testTwoLinesExternalCreateXmlFile() throws Exception {

		final File file = createTmpFileFromCommentsAroundThisMethod();

		final String content = FileUtils.readFileToString(file, UTF_8);

		assertEquals("<principal>Hello World!</principal>",
				normalizeSpace(content));
	}

	// <principal>Hello
	// World!</principal>

	@Test
	public void testCreateXmlFileFromAnotherMethod() throws Exception {

		final File file = createTmpFileFromCommentsAroundThisMethod("testSimpleCreateXmlFile");

		assertEquals(new File("target"), file.getParentFile());

		final String content = FileUtils.readFileToString(file, UTF_8);

		assertEquals("<helloWorld/>", content.trim());
	}

	@Test
	public void testExternalCreateXmlFile() throws Exception {

		final File file = createTmpFileFromCommentsAroundThisMethod();

		assertEquals(new File("target"), file.getParentFile());

		final String content = FileUtils.readFileToString(file, UTF_8);

		assertEquals("<empty/>", content.trim());
	}

	// <empty/>
}
