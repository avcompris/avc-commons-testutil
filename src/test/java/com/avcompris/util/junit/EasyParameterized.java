package com.avcompris.util.junit;

import static java.util.Arrays.asList;
import static org.apache.commons.lang3.ArrayUtils.EMPTY_OBJECT_ARRAY;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;


/**
 * simple tests for... tests!
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2009 ©
 */
@RunWith(AvcParameterized.class)
public class EasyParameterized {

	/**
	 * constructor.
	 */
	public EasyParameterized() {

		// nothing here
	}

	@Test
	public void testNothing() {

		// nothing here
	}

	/**
	 * @return a one-length array with nothing
	 * 
	 * @throws Exception
	 */
	@Parameters
	public static Collection<?> parameters() throws Exception {

		final Object nothing = EMPTY_OBJECT_ARRAY;

		return asList(nothing);
	}
}
