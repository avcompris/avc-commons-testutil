package com.avcompris.util.reflect;

import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.avcompris.util.junit.AvcFileAssert;
import com.avcompris.util.junit.AvcMatchers;
import com.avcompris.util.junit.AvcParameterized;
import com.avcompris.util.junit.IOTestUtils;
import com.avcompris.util.junit.JvyamlTestUtils;

/**
 * tests on Java classes in the "<code>avc-commons-testutil</code>" project, to check
 * if the Log4J logger declarations are normalized.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2009 ©
 */
@RunWith(AvcParameterized.class)
public class AvcCommonsTestutilLog4JLoggerTest extends AbstractLog4JLoggerTest {

	/**
	 * constructor.
	 * 
	 * @param clazz the Java class to test
	 */
	public AvcCommonsTestutilLog4JLoggerTest(final Class<?> clazz)
			throws Exception {

		super(clazz);
	}

	/**
	 * @return all the Java methods to test.
	 */
	@Parameters
	public static Collection<?> parameters() throws Exception {

		return parametersScanProject(AvcFileAssert.class, IOTestUtils.class,
				JvyamlTestUtils.class, AvcMatchers.class,
				AvcParameterized.class);
	}
}
