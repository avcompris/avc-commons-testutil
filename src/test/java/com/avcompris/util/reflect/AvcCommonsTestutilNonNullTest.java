package com.avcompris.util.reflect;

import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.avcompris.common.annotation.Nullable;
import com.avcompris.util.junit.AvcFileAssert;
import com.avcompris.util.junit.AvcMatchers;
import com.avcompris.util.junit.AvcParameterized;
import com.avcompris.util.junit.EasyParameterized;
import com.avcompris.util.junit.IOTestUtils;
import com.avcompris.util.junit.JvyamlTestUtils;

/**
 * tests on Java classes in the "<code>avc-commons-testutil</code>" project, to check
 * if the method parameters that are not flagged
 * as {@link Nullable} are checked for <code>null</code>-values.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2009 ©
 */
@RunWith(AvcParameterized.class)
public class AvcCommonsTestutilNonNullTest extends AbstractNonNullTest {

	/**
	 * constructor.
	 * 
	 * @param holder the instance+member to test
	 */
	public AvcCommonsTestutilNonNullTest(final MemberHolder holder)
			throws Exception {

		super(holder);
	}

	/**
	 * @return all the Java methods to test.
	 */
	@Parameters
	public static Collection<?> parameters() throws Throwable {

		return parametersScanProject(AvcFileAssert.class, IOTestUtils.class,
				JvyamlTestUtils.class, AvcMatchers.class, new AvcParameterized(
						EasyParameterized.class));
	}
}
