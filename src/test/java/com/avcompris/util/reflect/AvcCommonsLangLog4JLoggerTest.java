package com.avcompris.util.reflect;

import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.avcompris.util.AbstractUtils;
import com.avcompris.util.AvcIOUtils;
import com.avcompris.util.CharacterUtils;
import com.avcompris.util.DateUtils;
import com.avcompris.util.ExceptionUtils;
import com.avcompris.util.PropertiesUtils;
import com.avcompris.util.TransformUtils;
import com.avcompris.util.XMLUtils;
import com.avcompris.util.XsltFileURIResolver;
import com.avcompris.util.XsltResourceURIResolver;
import com.avcompris.util.YamlUtils;
import com.avcompris.util.junit.AvcParameterized;

/**
 * tests on Java classes in the "<code>avc-commons-lang</code>" project, to check
 * if the Log4J logger declarations are normalized.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2009 ©
 */
@RunWith(AvcParameterized.class)
public class AvcCommonsLangLog4JLoggerTest extends AbstractLog4JLoggerTest {

	/**
	 * constructor.
	 * 
	 * @param clazz the Java class to test
	 */
	public AvcCommonsLangLog4JLoggerTest(final Class<?> clazz) throws Exception {

		super(clazz);
	}

	/**
	 * @return all the Java methods to test.
	 */
	@Parameters
	public static Collection<?> parameters() throws Exception {

		return parametersDoNotScanProject(AbstractUtils.class,
				AvcIOUtils.class, CharacterUtils.class, DateUtils.class,
				ExceptionUtils.class, PropertiesUtils.class,
				TransformUtils.class, XMLUtils.class,
				XsltFileURIResolver.class, XsltResourceURIResolver.class,
				YamlUtils.class);
	}
}
