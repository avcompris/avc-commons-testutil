package com.avcompris.util.reflect;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;

import java.lang.reflect.Method;
import java.util.Comparator;

import org.junit.Test;

import com.avcompris.common.annotation.Nullable;
import com.avcompris.lang.NullArgumentException;
import com.avcompris.util.reflect.AbstractNonNullTest.MemberHolder;

/**
 * tests on our ability to test for non-null parameters in methods.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2010 ©
 */
public class NonNullEngineTest {

	@Test
	public void testNoParam() throws Throwable {

		final MemberHolder holder = new MemberHolder(new MyClass(),
				MyClass.class.getMethod("noParam"));

		new AbstractNonNullTest(holder) { // empty
		}.testAllNonNullableParametersAreChecked();
	}

	@Test
	public void testOneCheckedNotNullParam() throws Throwable {

		final MemberHolder holder = new MemberHolder(new MyClass(),
				MyClass.class.getMethod("oneCheckedNotNullParam", String.class));

		new AbstractNonNullTest(holder) { // empty
		}.testAllNonNullableParametersAreChecked();
	}

	@Test(expected = AssertionError.class)
	public void testOneUncheckedNotNullParam() throws Throwable {

		final MemberHolder holder = new MemberHolder(new MyClass(),
				MyClass.class.getMethod("oneUncheckedNotNullParam",
						String.class));

		new AbstractNonNullTest(holder) { // empty
		}.testAllNonNullableParametersAreChecked();
	}

	@Test
	public void testOneNullableParam() throws Throwable {

		final MemberHolder holder = new MemberHolder(new MyClass(),
				MyClass.class.getMethod("oneCheckedNotNullParam", String.class));

		new AbstractNonNullTest(holder) { // empty
		}.testAllNonNullableParametersAreChecked();
	}

	@Test
	public void testWithComparator() throws Throwable {

		for (final Method method : MyComparator.class.getDeclaredMethods()) {

			final MemberHolder holder = new MemberHolder(new MyComparator(),
					method);

			new AbstractNonNullTest(holder) { // empty
			}.testAllNonNullableParametersAreChecked();
		}
	}

	public static class MyClass {

		public void noParam() {

			// do nothing
		}

		public void oneCheckedNotNullParam(final String s) {

			if (s == null) {
				throw new NullArgumentException("s");
			}

			// do nothing
		}

		public void oneUncheckedNotNullParam(final String s) {

			// do nothing
		}

		public void oneNullableParam(@Nullable final String s) {

			// do nothing
		}
	}

	public static class MyComparator implements Comparator<String> {

		@Override
		public int compare(final String s1, final String s2) {

			nonNullArgument(s1, "s1");
			nonNullArgument(s2, "s2");

			System.out.println("---" + s1);
			return 0;
		}
	}
}
