package com.avcompris.util.reflect;

import static java.lang.Thread.currentThread;

import java.io.File;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.avcompris.common.annotation.Nullable;
import com.avcompris.lang.NotImplementedException;
import com.avcompris.lang.NullArgumentException;
import com.avcompris.util.AbstractUtils;
import com.avcompris.util.AvcIOUtils;
import com.avcompris.util.AvcXMLSerializer;
import com.avcompris.util.CharacterUtils;
import com.avcompris.util.DateUtils;
import com.avcompris.util.ExceptionUtils;
import com.avcompris.util.ListenerUtils;
import com.avcompris.util.PropertiesUtils;
import com.avcompris.util.RowClosureUtils;
import com.avcompris.util.SnakeYAMLUtils;
import com.avcompris.util.TransformUtils;
import com.avcompris.util.XMLUtils;
import com.avcompris.util.XPathUtils;
import com.avcompris.util.XsltFileURIResolver;
import com.avcompris.util.XsltResourceURIResolver;
import com.avcompris.util.YamlUtils;
import com.avcompris.util.junit.AvcParameterized;
import com.avcompris.util.log.LogUtils;

/**
 * tests on Java classes in the "<code>com.avcompris.util</code>" package, to check
 * if the method parameters that are not flagged
 * as {@link Nullable} are checked for <code>null</code>-values.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2009 ©
 */
@RunWith(AvcParameterized.class)
public class AvcCommonsLangNonNullTest extends AbstractNonNullTest {

	/**
	 * constructor.
	 * 
	 * @param holder the instance+member to test
	 */
	public AvcCommonsLangNonNullTest(final MemberHolder holder)
			throws Exception {

		super(holder);
	}

	/**
	 * @return all the Java methods to test.
	 */
	@Parameters
	public static Collection<?> parameters() throws Exception {

		return parametersDoNotScanProject(AbstractUtils.class,
				AvcIOUtils.class, AvcXMLSerializer.class, CharacterUtils.class,
				DateUtils.class, ExceptionUtils.class, ListenerUtils.class,
				LogUtils.class,
				NotImplementedException.class,
				NullArgumentException.class,
				PropertiesUtils.class,
				// RowClosure.class, RowClosureWithReturn.class, // Skip interfaces
				RowClosureUtils.class, SnakeYAMLUtils.class,
				TransformUtils.class, XMLUtils.class, XPathUtils.class,
				new XsltFileURIResolver(new File("pom.xml")),
				new XsltResourceURIResolver(currentThread()
						.getContextClassLoader()), YamlUtils.class);
	}
}
