package com.avcompris.util.reflect;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;

/**
 * use this class when you want to explicitly declare
 * what instance to use for a given class.
 * 
 * @author David Andrianavalontsalama
 */
public class UseInstance {

	public static <T> UseInstance forClass(final Class<T> type,
			final T instance, final boolean addToTests) {

		return new UseInstance(type, instance, addToTests);
	}

	private UseInstance(
			final Class<?> type,
			final Object instance,
			final boolean addToTests) {

		this.type = nonNullArgument(type, "type");
		this.instance = nonNullArgument(instance, "instance");
		this.addToTests = addToTests;
	}

	final Class<?> type;
	final Object instance;
	final boolean addToTests;
}
