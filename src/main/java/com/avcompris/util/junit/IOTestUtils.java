package com.avcompris.util.junit;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;
import static org.apache.commons.io.FileUtils.readFileToString;
import static org.apache.commons.lang3.CharEncoding.UTF_8;

import java.io.File;

import com.avcompris.util.AbstractUtils;

/**
 * utilities for test classes that operate on files.
 * 
 * @author David Andriana Copyright Avantage Compris SARL 2008-2009 ©
 */

public abstract class IOTestUtils extends AbstractUtils {

	/**
	 * return a String created with the content of an XML File.
	 * 
	 * @param filename the file's name
	 * @return the created String
	 */
	public static String loadXmlFile(
			final String filename) throws Exception {

		nonNullArgument(filename, "filename");

		return loadXmlFile(new File(filename));
	}

	/**
	 * return a String created with the content of an XML File.
	 * 
	 * @param file the file to read
	 * @return the created String
	 */
	public static String loadXmlFile(
			final File file) throws Exception {

		nonNullArgument(file, "file");

		return readFileToString(file, UTF_8);
	}
}
