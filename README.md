# About avc-commons-testutil

This project contains common classes shared by the JUnit tests in Avantage Compris' open source Java projects.

Its parent project is [avc-commons-parent](https://gitlab.com/avcompris/avc-commons-parent/).

Project dependencies include:

  * [avc-commons-lang](https://gitlab.com/avcompris/avc-commons-lang/)

Projects which depend on this one include:

  * [avc-binding-common](https://gitlab.com/avcompris/avc-binding-common/)
  * [avc-binding-dom](https://gitlab.com/avcompris/avc-binding-dom/)
  * [avc-binding-yaml](https://gitlab.com/avcompris/avc-binding-yaml/)

This is the project home page, hosted on GitLab.

[API Documentation is here](https://maven.avcompris.com/avc-commons-testutil/apidocs/index.html).

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-commons-testutil/)
